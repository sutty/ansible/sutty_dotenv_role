Sutty dotenv Role
=================

Installs [dotenv](https://0xacab.org/sutty/dotenv) on the local user for
running commands obtaining secrets from an `.env` file.  Replaces
`environment` option on tasks.

Example Playbook
----------------

```yaml
---
- hosts: "sutty"
  strategy: "free"
  remote_user: "root"
  tasks:
  - name: "role"
    include_role:
      name: "sutty_dotenv"
  - name: ".env"
    copy:
      src: ".env"
      dest: ".env"
  - name: "run"
    command: "bin/dotenv command"
```

License
-------

MIT-Antifa
